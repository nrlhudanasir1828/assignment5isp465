function myFunction() {
  var name = document.forms["calculator"]["name"].value; 
  var height = document.forms["calculator"]["height"].value;
  var length = document.forms["calculator"]["length"].value;
  var width = document.forms["calculator"]["width"].value;
  var mode= document.forms["calculator"]["mode"].value;
  var type = document.forms["calculator"]["type"].value;
  var weight;
  var cost;

  weight = (length*width*height)/5000;

  alert("Parcel Volumetric and Cost Calculator"+"\nCustomer name: "+uppercase(name)+"\nLength: "
    +length+" cm"+"\nWidth: "+width+" cm"+"\nHeight: "+height+" cm"+"\nWeight: "+weight+" kg"+
    "\nMode: "+mode+"\nType: "+type+"\nDelivery cost: RM "+calculateCost(type,weight,mode));
}

function resetAlert()
{
  alert("The inputs will be reset");
}

function uppercase(name) {
  var uppercase = document.getElementById("name");
  uppercase.value = uppercase.value.toUpperCase();
  str = name.toUpperCase();
  return str;
}

function calculateCost(type,weight,mode)
{
  var cost;

  if (type=="Domestic")
  {
    if (weight<2.00)
    {
      if (mode=="Surface")
        cost = 7;
      else if (mode=="Air")
        cost = 10;
    }
    else
    {
      if (mode=="Surface")
        cost = 7 + ((weight-2.0)*1.5);
      else if (mode=="Air")
        cost = 10 + ((weight-2.0)*3);
    }
  }
  else if (type=="International")
  {
    if(weight<2.00)
    {
      if (mode=="Surface")
        cost = 20;
      else if (mode=="Air")
        cost = 50;
    }
    else
    {
      if(mode=="Surface")
        cost = 20 + ((weight-2.0)*3);
      else if (mode=="Air")
        cost = 50 + ((weight-2.0)*5);
    }
  }
  return cost;
}
